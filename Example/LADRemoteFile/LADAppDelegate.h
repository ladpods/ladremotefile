//
//  LADAppDelegate.h
//  LADRemoteFile
//
//  Created by yanndupuy on 02/26/2018.
//  Copyright (c) 2018 yanndupuy. All rights reserved.
//

@import UIKit;

@interface LADAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
