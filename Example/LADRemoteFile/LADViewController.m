// ---------------------------------------------------------------------------------
//
//  LADViewController.m
//  LADRemoteFile
//
//  Created by yanndupuy on 02/26/2018.
//  Copyright (c) 2018 yanndupuy. All rights reserved.
//
// ---------------------------------------------------------------------------------

#import "LADViewController.h"

#import <LADRemoteFile/LADRemoteFile.h>
// ---------------------------------------------------------------------------------

@interface LADViewController ()

@end

// ---------------------------------------------------------------------------------

@implementation LADViewController

// ---------------------------------------------------------------------------------
#pragma mark - View Life Cycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
}

// ---------------------------------------------------------------------------------

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

// ---------------------------------------------------------------------------------
#pragma mark - Test Methods
// ---------------------------------------------------------------------------------

- (void)downloadSomeRessources
{
    // download a json file
    [LADRemoteFile downloadFromUrl:@"https://ota.ladmedia.fr/mobiles/LADRemoteFile/test/menu_iPhone.json"
                      withFilename:@"menu_iPhone.json"];
    
    // download a image file
    [LADRemoteFile downloadFromUrl:@"https://ota.ladmedia.fr/mobiles/LADRemoteFile/test/1024x1024bb-public.png"
                      withFilename:@"1024x1024bb-public.png"];
    
    // download an html template file and valid there is some macro inside
    [LADRemoteFile downloadFromUrl:@"https://ota.ladmedia.fr/mobiles/LADRemoteFile/test/Template_DetailArticle.html"
                      withFilename:@"Template_DetailArticle.html"
                  andValuesToCheck:[NSArray arrayWithObjects:@"_HEADLINE_",@"_BODY_", nil]
                        completion:^{
                            NSLog(@"\n\n\n-Test file Template_DetailArticle.html is download\n\n\n\n");
                        }];
    
    // download a font
    [LADRemoteFile downloadFromUrl:@"https://ota.ladmedia.fr/mobiles/LADRemoteFile/test/Abel-Regular.ttf"
                      withFilename:@"Abel-Regular.ttf"
                        completion:nil];
    
    // download a file without provide a filename
    [LADRemoteFile downloadFromUrl:@"https://ota.ladmedia.fr/mobiles/LADRemoteFile/test/testurl.txt?dfhfdg=sfdsdssfsf"];
}

// ---------------------------------------------------------------------------------

- (void)loadSomeTestRessources
{
//    NSDictionary *macroToReplace = [NSDictionary dictionaryWithObject:@"Hello World"
//                                                               forKey:@"_HEADLINE_"];
//
//    NSString *htmlStr2 = [LADRemoteFile getContentForFile:@"Template_DetailArticle.html"
//                                            withParameter:macroToReplace];
//    NSLog(@"- Load Test htmlStr2: %@\n\n\n",htmlStr2);
    
    NSString *htmlStr = [LADRemoteFile getContentForFile:@"Template_DetailArticle.html"];
    NSLog(@"- Load Test htmlStr: %@\n\n\n",htmlStr);
    
//    NSString *htmlStrFromBundle = [LADRemoteFile getContentForFileFromAppBundle:@"Template_DetailArticle.html"];
//    NSLog(@"- Load Test htmlStrFromBundle: %@\n\n\n",htmlStrFromBundle);
    
    UIImage *img = [LADRemoteFile getImageName:@"1024x1024bb-public.png"];
    [self.imageViewDL setImage:img];
    NSLog(@"- Load Test img: %@\n\n\n",img.description);
    
//    UIImage *imgFromBundle = [LADRemoteFile getImageNameFromAppBundle:@"1024x1024bb-public.png"];
//    [self.imageViewDL setImage:imgFromBundle];
//    NSLog(@"- Load Test imgFromBundle: %@\n\n\n",imgFromBundle.description);
    
    NSDictionary *json = [LADRemoteFile getJsonFileWithName:@"menu_iPhone.json"];
    NSLog(@"- Load Test json: %@\n\n\n",json.description);
    
//    NSDictionary *jsonFromBundle = [LADRemoteFile getJsonFileWithNameFromAppBundle:@"menu_iPhone.json"];
//    NSLog(@"- Load Test jsonFromBundle: %@\n\n\n",jsonFromBundle.description);
    
}

// ---------------------------------------------------------------------------------
#pragma mark - Memory Methods

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// ---------------------------------------------------------------------------------
#pragma mark - action

- (IBAction)clickOnDownload:(id)sender
{
    [self downloadSomeRessources];
}

// ---------------------------------------------------------------------------------

- (IBAction)clickOnLoad:(id)sender
{
    [self loadSomeTestRessources];
}

// ---------------------------------------------------------------------------------
- (IBAction)clickOnWipe:(id)sender
{
    [LADRemoteFile wipeRemoteDirectory];
}

// ---------------------------------------------------------------------------------


@end
