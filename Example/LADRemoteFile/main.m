//
//  main.m
//  LADRemoteFile
//
//  Created by yanndupuy on 02/26/2018.
//  Copyright (c) 2018 yanndupuy. All rights reserved.
//

@import UIKit;
#import "LADAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LADAppDelegate class]));
    }
}
