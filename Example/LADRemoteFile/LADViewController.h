//
//  LADViewController.h
//  LADRemoteFile
//
//  Created by yanndupuy on 02/26/2018.
//  Copyright (c) 2018 yanndupuy. All rights reserved.
//

@import UIKit;

@interface LADViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *downloadClick;
@property (weak, nonatomic) IBOutlet UIButton *loadClick;
@property (weak, nonatomic) IBOutlet UIButton *wipeDirectoryClick;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewDL;

@end
