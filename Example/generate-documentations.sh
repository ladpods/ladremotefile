#!/bin/sh

#=======================================================================================
echo "Generation documentation with different tools"

#echo "documentation with jazzy"
#./generate-jazzy-documentation.sh

echo "documentation with headerdoc2html"
./generate-headerdoc2html-documentation.sh

echo "documentation with appledoc"
./generate-appledoc-documentation.sh
