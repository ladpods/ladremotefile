#!/bin/sh

#=======================================================================================
echo "Generation documentation with appledoc"

# Start constants  
company="Lagardere Digital France";  
companyID="com.lagardere";
companyURL="http://www.lagardere.com/";
target="iphoneos";
#target="macosx";
outputPath="../appledoc-documentation";
# End constants
/usr/local/bin/appledoc \
--project-name "LADRemoteFile" \
--project-company "${company}" \
--company-id "${companyID}" \
--docset-atom-filename "${company}.atom" \
--docset-feed-url "${companyURL}/${company}/%DOCSETATOMFILENAME" \
--docset-package-url "${companyURL}/${company}/%DOCSETPACKAGEFILENAME" \
--docset-fallback-url "${companyURL}/${company}" \
--no-install-docset \
--output "${outputPath}" \
--docset-platform-family "${target}" \
--logformat xcode \
--keep-intermediate-files \
--no-repeat-first-par \
--no-warn-invalid-crossref \
--exit-threshold 2 \
"../LADRemoteFile/Classes/"



