#!/bin/sh

#=======================================================================================
echo "Generation documentation with jazzy"

jazzy \
  --objc \
  --clean \
  --copyright Lagardere Digital France \
  --author Yann Dupuy \
  --module-version 1.0 \
  --module LADRemoteFile \
  --output ../jazzydocs/objc_output \
  --sdk iphonesimulator \
  --theme apple\
  --source-directory ../LADRemoteFile/ \
  
  #--framework-root ../LADRemoteFile/LADRemoteFile.h \
  #
  #--head "$(cat docs/custom_head.html)"