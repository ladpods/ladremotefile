# LADRemoteFile

# My Table of content
- [Description](#description)
- [Example](#example)
- [Installation](#installation)
- [Documentation](#documentation)
- [Implementation](#implementation)
    - [Get debug info](#debug_info)
    - [Download a ressource](#download_res)
    - [Content Loading](#content_fetch)
        - [HTML template / Text ressource](#html_test_res)
        - [Image ressource](#image_res)
        - [JSON file ressource](#json_res)
        - [Load ressources for debug](#res_debug)
- [Update pod development ](#pod_dev)




<div id='description'/>

## Description

'LADRemoteFile' pod is for manage 'remote ressources' which can be also present in 'App Bunlde' as default value (html template, json , image ,...).

The prupose of this class is to offer the following function (throught class metho) :

- download ressource (image, html, text, json)
- load this ressource from remote directory or App Bundle 
- manage all ressource on one place
- provide fallback method on data loading in order to use default value provide in 'App Bundle'


<div id='example'/>

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

- langage : objective-c
- iOS > 9.0
- dependency : AFNetwork 3.x

<div id='installation'/>

## Installation

LADRemoteFile is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LADRemoteFile'
```

<div id='documentation'/>

## Documentation

Classe docummentation avalaible at the following urls :

- [headerdoc2html](./headerdoc2html-documentation/LADRemoteFile_h/index.html)
- [appledoc](./appledoc-documentation/html/index.html)


<div id='implementation'/>

## Implementation

header importation :

```objc
#import <LADRemoteFile/LADRemoteFile.h>
```


<div id='debug_info'/>

### Get debug info :

for set debug log level :

```
// LADRemoteFileDebugModeNone - default log level 
[[LADRemoteFile sharedInstance] setDebugMode:LADRemoteFileDebugModeNone];
```

log level value :

- LADRemoteFileDebugModeNone
- LADRemoteFileDebugModeInfo
- LADRemoteFileDebugModeError
- LADRemoteFileDebugModeVerbose

Print some debug info (like directory where ressource are write : )

```objc
// Display info about 'LADRemoteFile configuration'
    [LADRemoteFile debugInfo];
```

output :

```

 -[LADRemoteFile] Info:
 -[LADRemoteFile] - remoteDirectory name : remote
 -[LADRemoteFile] - DebugMode : LADRemoteFileDebugModeVerbose
 -[LADRemoteFile] - remoteDirectory path : /Users/yann.dupuy/Library/Developer/CoreSimulator/Devices/D75E1EA6-2B9B-4A3E-BEED-EF72B297006E/data/Containers/Data/Application/086D3EBC-B436-4FCC-9DB4-61302CDBDBE6/Library/Caches/remote

```

Directory where ressources are write can be change (always write in 'Library/Cache/#DIR_OF_YOUR_CHOICE#')

By default it's write in : 'Library/Cache/remote'


```
// Define a specific directory - default is 'remote'
    [[LADRemoteFile sharedInstance] setRemoteDirectoryName:@"LADRemote"];

```

<div id='download_res'/>

### Download a ressource 

Download a file at the provide 'url'  and save it with the provide name, if inside the downloaded file we found values presence we want check. At the end call a completion block for inform process is finish

*note :*
If there is an error, file downloaded will be rename as filenameProvided_error for debug prupose


```
// download an html template file and valid there is some macro inside
    [LADRemoteFile downloadFromUrl:@"https://ota.ladmedia.fr/mobiles/LADRemoteFile/test/Template_DetailArticle.html"
                      withFilename:@"Template_DetailArticle.html"
                  andValuesToCheck:[NSArray arrayWithObjects:@"_HEADLINE_",@"_BODY_", nil]
                        completion:^{
                            NSLog(@"\n\n\n-Test file Template_DetailArticle.html is download\n\n\n\n");
                        }];

```
download function variation :

```
// download a json file
    [LADRemoteFile downloadFromUrl:@"https://ota.ladmedia.fr/mobiles/LADRemoteFile/test/menu_iPhone.json"
                      withFilename:@"menu_iPhone.json"];
                      


```
For more info see class documentation

<div id='content_fetch'/>

### Content Loading

<div id='html_test_res'/>

#### HTML template / Text ressource

Try to load a text file (html, txt, etc) from remote directory and replace  macro/value couple if  find inside the content

Macro to replace are optionnal.

If ressource not found in remote directory try to find a default ressource in app bundle. If neither found return an empty string @""

```
NSDictionary *macroToReplace = [NSDictionary dictionaryWithObject:@"Hello World"
                                                               forKey:@"_HEADLINE_"];
    
    NSString *htmlStr = [LADRemoteFile getContentForFile:@"Template_DetailArticle.html"];
    
[LADRemoteFile getContentForFile:<#(NSString *)#> withParameter:<#(NSDictionary *)#>]

```

<div id='image_res'/>

#### Image ressource

Try to load an image ressource form remote directory if not found try find find similar image name in App Bundle and return it. If nothing found return nil value

```
UIImage *img = [LADRemoteFile getImageName:@"1024x1024bb-public.png"];

```

<div id='json_res'/>

#### JSON file ressource

Try to load a json file from remote directory 

Macro to replace are optionnal.

If ressource not found in remote directory try to find a default ressource in app bundle. If neither found return nil value


```
NSDictionary *json = [LADRemoteFile getJsonFileWithName:@"menu_iPhone.json"];

```

<div id='res_debug'/>

#### Load ressources for debug 

In order to debug some remote ressource before put them in production we can use some loading method which only use default file available in 'App Bundle'

Example 

```
[LADRemoteFile getContentForFileFromAppBundle:@"Template_DetailArticle.html"];

[LADRemoteFile getImageNameFromAppBundle:@"1024x1024bb-public.png"];

[LADRemoteFile getJsonFileWithNameFromAppBundle:@"menu_iPhone.json"];

```

Force to load ressource from App bunlde in any case can be done throught 'LADRemote singleton'

BE CAREFUL for use it for debug purpose only

```
#ifdef DEBUG
    // Force to use App Bundle ressource
    [[LADRemoteFile sharedInstance] setForceAppBundleRessource:TRUE];
#endif

```

<div id='pod_dev'/>

## Update pod development 

### Steps for update pod

* Step 1 - code your update in 'development' branch
* Step 2 - dev finish : update pod version in podspec file
* Step 3 - commit everything 
* Step 4 - merge 'development' branch into master
* Step 5 - Tag master branch with the smae version as define in podspec
* Step 6 - (in terminal) publish your pod update with the following command line :

```
pod repo push ladtech-mobile-podspec LADRemoteFile.podspec --verbose --use-libraries --allow-warnings

```

* Step 7 - that all you can now update the pod in all project !!!


## Author

yanndupuy, Yann.DUPUY@Lagardere-Active.com

## License

LADRemoteFile is available under the MIT license. See the LICENSE file for more info.
