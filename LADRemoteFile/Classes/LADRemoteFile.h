
// ---------------------------------------------------------------------------------
//
//  LADRemoteFile.h
//
//  Created by DUPUY Yann on 02/26/2018.
//  Copyright (c) 2018 Lagardere Digital France. All rights reserved.
//  Updated by Laurine Baillet on 17/01/2018
//
// ---------------------------------------------------------------------------------

/*!
 @header LADRemoteFile.h
 hoge
 */

#import <Foundation/Foundation.h>

// ---------------------------------------------------------------------------------

typedef enum {
    
    LADRemoteFileDebugModeNone,
    LADRemoteFileDebugModeInfo,
    LADRemoteFileDebugModeError,
    LADRemoteFileDebugModeVerbose
    
} LADRemoteFileDebugMode;

// ---------------------------------------------------------------------------------

/*!
 @header
 @abstract <code>LADRemoteFile</code> class package file
 
 this class aim to manage download and use of remote ressources which can be also present in app bunlde as default value
 
 @discussion
 This file contains the <code>LADRemoteFile</code> class, which is the base class for managed remote ressources (html, json, image, etc)
 See the class documentation below for more details.
 */
@interface LADRemoteFile : NSObject

/// @brief Define debug log level
@property (nonatomic, assign)   LADRemoteFileDebugMode      debugMode;

// ---------------------------------------------------------------------------------
#pragma mark - Init
// ---------------------------------------------------------------------------------

/*!
 @brief Get singleton instance for configuration
 */
+ (instancetype)sharedInstance;

// ---------------------------------------------------------------------------------
#pragma mark - configuration Methods
// ---------------------------------------------------------------------------------

/*!
 @brief Define directory name where files are write
 @param remoteDirectoryName directory name to set
 */
- (void)setRemoteDirectoryName:(NSString *)remoteDirectoryName;

// ---------------------------------------------------------------------------------

/*!
 @brief Force to load ressource from App Bundle
 @param forceUseAppBundleRessource bool flag value by default value is set as False
 */
- (void)setForceAppBundleRessource:(BOOL)forceUseAppBundleRessource;

// ---------------------------------------------------------------------------------

/*!
 @brief Remove remote directory
 */
+ (void)wipeRemoteDirectory;

// ---------------------------------------------------------------------------------
#pragma mark - Download Methods
// ---------------------------------------------------------------------------------

/*!
 @brief It save a file with last component has name, if server is not available, the default template will be use if available.
 @param  url The url of the file on the server
 */
+ (void)downloadFromUrl:(NSString *)url;

// ---------------------------------------------------------------------------------

/*!
 @brief It save a file with his name, if server is not available, the default template will be use.
 @param  url The url of the file on the server
 @param filename the file name with the extension
 */
+ (void)downloadFromUrl:(NSString *)url
           withFilename:(NSString *)filename;

// ---------------------------------------------------------------------------------

/*!
 @brief It save a file with his name, if server is not available, the default template will be use.
 @param  url The url of the file on the server
 @param filename the file name with the extension
 @param completion block to execute at the end
 */
+ (void)downloadFromUrl:(NSString *)url
           withFilename:(NSString *)filename
             completion:(void (^)(void))completion;

// ---------------------------------------------------------------------------------

/*!
 @brief It save a file with his name, if server is not available, the default template will be use.
 @param  url The url of the file on the server
 @param filename the file name with the extension
 @param valuesToCheck An array of NSString that has to be in the template saved (check of the template return by the server). Can be nil or empty
 @param completion block to execute at the end
 */
+ (void)downloadFromUrl:(NSString *)url
           withFilename:(NSString *)filename
       andValuesToCheck:(NSArray *)valuesToCheck
             completion:(void (^)(void))completion;

// ---------------------------------------------------------------------------------
#pragma mark - Load Ressources Methods
// ---------------------------------------------------------------------------------

/*!
 @brief get the content of a file (html,json...) completed with parameters (local or remote)
 @param filename The name of the file requested with the extension
 @return A string created by reading data from the file named by path. If the file can’t be opened or there is an encoding error, returns empty string
 */
+ (NSString *)getContentForFile:(NSString *)filename;

// ---------------------------------------------------------------------------------

/*!
 @brief Get the content of a file (html,json...) completed with parameters (local or remote)
 @param fileName The name of the file requested with the extension
 @param parameters to replace occurences of string in html. Key = occurence to replace (_BODY_),
 value = the value for the occurence (<div>bla bla</div>)
 @return A string created by reading data from the file named by path. If the file can’t be opened or there is an encoding error, returns empty string
 */
+ (NSString *)getContentForFile:(NSString *)fileName withParameter:(NSDictionary *)parameters;

// ---------------------------------------------------------------------------------

/*!
 @brief Get content of file store in app bundle
 @param filename name of ressource to load
 @return A string created by reading data from the file named by path. If the file can’t be opened or there is an encoding error, returns empty string
 */
+ (NSString *)getContentForFileFromAppBundle:(NSString *)filename;

// ---------------------------------------------------------------------------------

/*!
@brief Get content of file store in app bundle
@param filename name of ressource to load
@param parameters to replace occurences of string in html. Key = occurence to replace (_BODY_),
@return A string created by reading data from the file named by path. If the file can’t be opened or there is an encoding error, returns empty string
*/
+ (NSString *)getContentForFileFromAppBundle:(NSString *)filename withParameter:(NSDictionary *)parameters;

// ---------------------------------------------------------------------------------
#pragma mark - Image Load methods
// ---------------------------------------------------------------------------------

/*!
 @brief Get UIImage ressource loaded from remote directory. If ressource not found and present in App Bundle
 return this image ressource
 @param imageName image filename
 @return UIImage object found, if not found return nil
 */
+ (UIImage *)getImageName:(NSString *)imageName;

// ---------------------------------------------------------------------------------

/*!
 @brief Get UIImage load from App bundle if found
 @param imageName image filename
 @return UIImage object found, if not found return nil
 */
+ (UIImage *)getImageNameFromAppBundle:(NSString *)imageName;

// ---------------------------------------------------------------------------------
#pragma mark - JSON Load methods
// ---------------------------------------------------------------------------------

/*!
 @brief Get Json file representation loaded from App Bundle
 @param jsonFilename ressource filename
 @return NSDictionary object found, if not found return nil
 */
+ (NSDictionary *)getJsonFileWithName:(NSString*)jsonFilename;

// ---------------------------------------------------------------------------------

/*!
@brief Get Json file representation loaded from remote directory. If ressource not found and present in App Bundle
return this ressource
@param jsonFilename ressource filename
@return NSDictionary object found, if not found return nil
*/
+ (NSDictionary *)getJsonFileWithNameFromAppBundle:(NSString*)jsonFilename;

// ---------------------------------------------------------------------------------
#pragma mark - Debug methods
// ---------------------------------------------------------------------------------

/*!
 @brief Print Information about LADRemoteFile configuration
 - debug Mode
 - remote directory name
 - remote directory path
 */
+ (void)debugInfo;

// ---------------------------------------------------------------------------------

@end




