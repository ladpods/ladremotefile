// ---------------------------------------------------------------------------------
//
//  LADRemoteFile.m
//  LADRemoteFile
//
//  Created by DUPUY Yann on 02/26/2018.
//  Copyright (c) 2018 Lagardere Digital France. All rights reserved.
//
// ---------------------------------------------------------------------------------

#import "LADRemoteFile.h"

// Network
#import <AFNetworking/AFHTTPSessionManager.h>

// ---------------------------------------------------------------------------------
#pragma mark - Interface
// ---------------------------------------------------------------------------------

@interface LADRemoteFile (/* Private */)

@property (nonatomic, strong) NSString      *remoteDirectoryName;
@property (nonatomic, assign) BOOL          forceAppBundleRessource;

@end

// ---------------------------------------------------------------------------------
#pragma mark - Implementation
// ---------------------------------------------------------------------------------

@implementation LADRemoteFile

// ---------------------------------------------------------------------------------
#pragma mark - Init
// ---------------------------------------------------------------------------------

/*!
 @brief Get singleton instance for configuration
 */
+ (instancetype)sharedInstance
{
    static LADRemoteFile *sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        sharedManager = [[self alloc] init];
        sharedManager.debugMode = LADRemoteFileDebugModeNone;
        sharedManager.remoteDirectoryName = @"remote";
        
    });
    
    return sharedManager;
}

// ---------------------------------------------------------------------------------
#pragma mark - configuration Methods
// ---------------------------------------------------------------------------------

/*!
 @brief Define directory name where files are write
 @param remoteDirectoryName directory name to set
 */
- (void)setRemoteDirectoryName:(NSString *)remoteDirectoryName
{
    _remoteDirectoryName = remoteDirectoryName;
}

// ---------------------------------------------------------------------------------

/*!
 @brief Force to load ressource from App Bundle
 @param forceUseAppBundleRessource bool flag value by default value is set as False
 */
- (void)setForceAppBundleRessource:(BOOL)forceUseAppBundleRessource
{
    _forceAppBundleRessource = forceUseAppBundleRessource;
}

// ---------------------------------------------------------------------------------

/*!
 @brief Remove remote directory
 */
+ (void)wipeRemoteDirectory
{
    NSError* error;
    NSString *remoteDirectoryPath = [LADRemoteFile getRemoteDirectoryPath];
    BOOL isDir;
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:remoteDirectoryPath
                                             isDirectory:&isDir])
    {
        bool status = [[NSFileManager defaultManager] removeItemAtPath:remoteDirectoryPath
                                                                 error:&error];
        
        if (status == true)
        {
            if ([LADRemoteFile sharedInstance].debugMode > LADRemoteFileDebugModeNone)
            {
                NSLog(@"-[LADRemote] remote directory is deleted (path %@)\n\n",remoteDirectoryPath);
            }
        }
        else if ([LADRemoteFile sharedInstance].debugMode > LADRemoteFileDebugModeNone)
        {
            NSLog(@"-[LADRemote] - ERROR : failed to delete remote directory (path %@)\n\n",remoteDirectoryPath);
        }
        
        if (error != nil)
        {
            if ([LADRemoteFile sharedInstance].debugMode >= LADRemoteFileDebugModeError)
            {
                NSLog(@"-[LADRemote] - ERROR : %@\n\n",error.description);
            }
        }
    }
}

// ---------------------------------------------------------------------------------
#pragma mark - Download Methods
// ---------------------------------------------------------------------------------

/*!
 @brief It save a file with last component has name, if server is not available, the default template will be use if available.
 @param  url The url of the file on the server
 */
+ (void)downloadFromUrl:(NSString *)url
{
    if (url != nil && url.length > 0 && [url hasPrefix:@"http"])
    {
        NSString *filename = [[NSURL URLWithString:url] lastPathComponent];
        
        if (filename != nil && filename.length > 0)
        {
            [LADRemoteFile downloadFromUrl:url
                              withFilename:filename];
        }
        else if([LADRemoteFile sharedInstance].debugMode >LADRemoteFileDebugModeNone)
        {
            NSLog(@"-[LADRemoteFile] - ERROR : no filename found for save this file : %@ | url : %@\n\n",filename,url);
        }
    }
    else if([LADRemoteFile sharedInstance].debugMode >LADRemoteFileDebugModeNone)
    {
        NSLog(@"-[LADRemoteFile] - ERROR : provide url is not valid\n\n");
    }
    
}

// ---------------------------------------------------------------------------------

/*!
 @brief It save a file with his name, if server is not available, the default template will be use.
 @param  url The url of the file on the server
 @param filename the file name with the extension
 */
+ (void)downloadFromUrl:(NSString *)url
           withFilename:(NSString *)filename
{
    [LADRemoteFile downloadFromUrl:url
                      withFilename:filename
                  andValuesToCheck:nil
                        completion:nil];
}

// ---------------------------------------------------------------------------------

/*!
 @brief It save a file with his name, if server is not available, the default template will be use.
 @param  url The url of the file on the server
 @param filename the file name with the extension
 @param completion block to execute at the end
 */
+ (void)downloadFromUrl:(NSString *)url
           withFilename:(NSString *)filename
             completion:(void (^)(void))completion
{
    [LADRemoteFile downloadFromUrl:url
                      withFilename:filename
                  andValuesToCheck:nil
                        completion:completion];
    
}

// ---------------------------------------------------------------------------------

/*!
 @brief It save a file with his name, if server is not available, the default template will be use.
 @param  url The url of the file on the server
 @param filename the file name with the extension
 @param valuesToCheck An array of NSString that has to be in the template saved (check of the template return by the server). Can be nil or empty
 @param completion block to execute at the end
 */
+ (void)downloadFromUrl:(NSString *)url
           withFilename:(NSString *)filename
       andValuesToCheck:(NSArray *)valuesToCheck
             completion:(void (^)(void))completion
{
    if (!url || !filename)
    {
        if (completion != nil)
        {
            completion();
        }
        
        return;
    }
    
    NSString *webStringURL = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
    
    webStringURL = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    // Create directories
    NSString *path = [LADRemoteFile getRemoteDirectoryPath];
    [self createDirectoryAtPath:path];
    
    //Create urls
    NSURL *fileDirectoryPath = [NSURL fileURLWithPath:path];
    NSURL *fileURL = [NSURL URLWithString:webStringURL];
    
    //#ifdef DEBUG
    ////    Test with any server response
    //    fileURL = [NSURL URLWithString:@"https://httpstat.us/503"];
    //#endif
    
    NSURLRequest *request = [NSURLRequest requestWithURL:fileURL];
    
    // Download the file
    NSURLSessionDownloadTask *downloadTask;
    downloadTask = [[LADRemoteFile httpSessionManager] downloadTaskWithRequest:request
                                                                      progress:nil
                                                                   destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response)
    {
        NSURL* destination = [fileDirectoryPath URLByAppendingPathComponent:filename];
        [[NSFileManager defaultManager] removeItemAtURL:destination error:nil];
        
        if ([LADRemoteFile sharedInstance].debugMode > LADRemoteFileDebugModeNone)
        {
            NSLog(@"\n\n-[LADRemoteFile] : download template with name : %@\n\n",filename);
        }
        
        return destination;
        
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
        NSURL* destination = [fileDirectoryPath URLByAppendingPathComponent:filename];
        
        if(httpResponse.statusCode == 200)
        {
            //In completion block if not : move cannot be executed on a file not writen yet
            if([LADRemoteFile checkValues:valuesToCheck inTemplateAtPath:destination])
            {
            }
            else
            {
                if ([LADRemoteFile sharedInstance].debugMode >= LADRemoteFileDebugModeError)
                {
                    NSLog(@"\n\n-[LADRemoteFile] ERROR : le template téléchargé %@ ne correspond pas à celui demandé.\n Vérifiez si le fichier mis en ligne n'a pas été modifié.\n\n", filename);
                }
                
                [LADRemoteFile renameErrorFileWithName:filename];
            }
        }
        else
        {
            [LADRemoteFile renameErrorFileWithName:filename];
            
            if ([LADRemoteFile sharedInstance].debugMode > LADRemoteFileDebugModeNone)
            {
                NSLog(@"\n\n-[LADRemoteFile] : response status code: %ld, local template will be used.\n\n", (long)httpResponse.statusCode);
            }
        }
        
        if (completion != nil)
        {
            completion();
        }
        
    }];
    
    [downloadTask resume];
}

// ---------------------------------------------------------------------------------
#pragma mark - Load Ressources Methods
// ---------------------------------------------------------------------------------

/*!
 @brief get the content of a file (html,json...) completed with parameters (local or remote)
 @param filename The name of the file requested with the extension
 @return A string created by reading data from the file named by path. If the file can’t be opened or there is an encoding error, returns empty string
 */
+ (NSString *)getContentForFile:(NSString *)filename
{
    return [LADRemoteFile getContentForFile:filename
                              withParameter:nil];
    
}

// ---------------------------------------------------------------------------------

/*!
 @brief get the content of a file (html,json...) completed with parameters (local or remote)
 @param fileName The name of the file requested with the extension
 @param parameters to replace occurences of string in html. Key = occurence to replace (_BODY_),
 value = the value for the occurence (<body>bla bla</body>)
 @return A string created by reading data from the file named by path. If the file can’t be opened or there is an encoding error, returns empty string
 */
+ (NSString *)getContentForFile:(NSString *)fileName withParameter:(NSDictionary *)parameters
{
    NSString *content;
    NSString *filePath = [LADRemoteFile getFilePathFromRemoteDirectory:fileName];
    
    // if file not present in remote directory try to load from app bundle
    if(![[NSFileManager defaultManager] fileExistsAtPath:filePath] || ([LADRemoteFile sharedInstance].forceAppBundleRessource == TRUE) )
    {
        filePath = [LADRemoteFile getFilePathFromAppBundle:fileName];
    }
    
    content = [LADRemoteFile getContentFromFilePath:filePath
                                      withParameter:parameters];
    
    return content;
}

// ---------------------------------------------------------------------------------

/*!
 @brief Get content of file store in app bundle
 @param filename name of ressource to load
 @return A string created by reading data from the file named by path. If the file can’t be opened or there is an encoding error, returns empty string
 */
+ (NSString *)getContentForFileFromAppBundle:(NSString *)filename
{
    return [LADRemoteFile getContentForFileFromAppBundle:filename
                                           withParameter:nil];
}

// ---------------------------------------------------------------------------------

/*!
 @brief Get content of file store in app bundle
 @param filename name of ressource to load
 @return A string created by reading data from the file named by path. If the file can’t be opened or there is an encoding error, returns empty string
 */
+ (NSString *)getContentForFileFromAppBundle:(NSString *)filename withParameter:(NSDictionary *)parameters
{
    NSString *content;
    NSString *localFilepath = [LADRemoteFile getFilePathFromAppBundle:filename];
    
    content = [LADRemoteFile getContentFromFilePath:localFilepath
                                      withParameter:parameters];
    
    return content;
}

// ---------------------------------------------------------------------------------
#pragma mark - Image Load methods
// ---------------------------------------------------------------------------------

/*!
 @brief Get UIImage ressource loaded from remote directory. If ressource not found and present in App Bundle
 return this image ressource
 @param imageName image filename
 @return UIImage object found, if not found return nil
 */
+ (UIImage *)getImageName:(NSString *)imageName
{
    // try to load from remote directory
    UIImage *image = nil;
    NSString *imagePath  = [LADRemoteFile getFilePathFromRemoteDirectory:imageName];
    
    if (imagePath != nil && imagePath.length > 0)
    {
        if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath] == TRUE)
        {
            image = [UIImage imageWithContentsOfFile:imagePath];
            
            if ([LADRemoteFile sharedInstance].debugMode > LADRemoteFileDebugModeNone)
            {
                NSLog(@"-[LADRemoteFile - Info : ressource '%@' load from remote directory",imageName);
            }
        }
        else if ([LADRemoteFile sharedInstance].debugMode > LADRemoteFileDebugModeNone)
        {
            NSLog(@"-[LADRemoteFile - Info : ressource '%@' not in remote directory",imageName);
        }
    }
    
    // try to load from App Bundle
    if (image == nil || ([LADRemoteFile sharedInstance].forceAppBundleRessource == TRUE))
    {
        image = [LADRemoteFile getImageNameFromAppBundle:imageName];
    }
    
    return image;
}

// ---------------------------------------------------------------------------------

/*!
 @brief Get UIImage load from App bundle if found
 @param imageName image filename
 @return UIImage object found, if not found return nil
 */
+ (UIImage *)getImageNameFromAppBundle:(NSString *)imageName
{
    UIImage *seekImage = [UIImage imageNamed:imageName];
    
    if (seekImage == nil && ([LADRemoteFile sharedInstance].debugMode > LADRemoteFileDebugModeNone))
    {
        NSLog(@"-[LADRemoteFile - ERROR : ressource '%@' not found",imageName);
    }
    else if ([LADRemoteFile sharedInstance].debugMode > LADRemoteFileDebugModeNone)
    {
        NSLog(@"-[LADRemoteFile - Info : ressource '%@' load from App bundle",imageName);
    }
    
    return seekImage;
}

// ---------------------------------------------------------------------------------
#pragma mark - JSON Load methods
// ---------------------------------------------------------------------------------

/*!
 @brief Get Json file representation loaded from App Bundle
 @param jsonFilename ressource filename
 @return NSDictionary object found, if not found return nil
 */
+ (NSDictionary *)getJsonFileWithName:(NSString*)jsonFilename
{
    // try to load from remote directory
    NSDictionary *jsonDict;
    NSString *jsonFilepath = [LADRemoteFile getFilePathFromRemoteDirectory:jsonFilename];
    
    if (jsonFilepath != nil && jsonFilepath.length > 0 && ([LADRemoteFile sharedInstance].forceAppBundleRessource == FALSE))
    {
        jsonDict = [LADRemoteFile getJsonAtPath:jsonFilepath];
        
        if (jsonDict != nil && ([LADRemoteFile sharedInstance].debugMode >= LADRemoteFileDebugModeInfo))
        {
            NSLog(@"-[LADRemoteFile] - INFO : ressource %@ load from remote directory",jsonFilename);
        }
    }
    
    // try to load from App Bundle
    if (jsonDict == nil)
    {
        if ([LADRemoteFile sharedInstance].debugMode >= LADRemoteFileDebugModeInfo)
        {
            NSLog(@"-[LADRemoteFile] - ERROR :  json ressource %@ not present in remote directory, try to load from App Bundle",jsonFilename);
        }
        
        jsonDict = [LADRemoteFile getJsonFileWithNameFromAppBundle:jsonFilename];
        
        if (jsonDict == nil && ([LADRemoteFile sharedInstance].debugMode >= LADRemoteFileDebugModeInfo))
        {
            NSLog(@"-[LADRemoteFile] - ERROR :  json ressource %@ not present (Remote & Bundle dir)",jsonFilename);
        }
    }
    
    return jsonDict;
}

// ---------------------------------------------------------------------------------

/*!
 @brief Get Json file representation loaded from remote directory. If ressource not found and present in App Bundle
 return this ressource
 @param jsonFilename ressource filename
 @return NSDictionary object found, if not found return nil
 */
+ (NSDictionary *)getJsonFileWithNameFromAppBundle:(NSString*)jsonFilename
{
    NSDictionary *jsonDict;
    NSString *jsonFilepath = [LADRemoteFile getFilePathFromAppBundle:jsonFilename];
    
    if (jsonFilepath != nil && jsonFilepath.length > 0)
    {
        jsonDict = [LADRemoteFile getJsonAtPath:jsonFilepath];
    }
    
    if (jsonDict != nil )
    {
        if (([LADRemoteFile sharedInstance].debugMode >= LADRemoteFileDebugModeInfo))
        {
            NSLog(@"-[LADRemoteFile] - INFO : ressource %@ load from App Bundle",jsonFilename);
        }
    }
    else
    {
        if ([LADRemoteFile sharedInstance].debugMode >= LADRemoteFileDebugModeInfo)
        {
            NSLog(@"-[LADRemoteFile] - ERROR : ressource %@ not found in App Bundle",jsonFilename);
        }
    }
    
    return jsonDict;
}

// ---------------------------------------------------------------------------------
#pragma mark - Debug methods
// ---------------------------------------------------------------------------------

/*!
 @brief Print Information about LADRemoteFile configuration
 - debug Mode
 - remote directory name
 - remote directory path
 */
+ (void)debugInfo
{
    NSLog(@"\n\n");
    NSLog(@"-[LADRemoteFile] Info:\n\n");
    NSLog(@"-[LADRemoteFile] - remoteDirectory name : %@\n",[LADRemoteFile sharedInstance].remoteDirectoryName);
    NSLog(@"-[LADRemoteFile] - DebugMode : %@\n",[LADRemoteFile debugModeToString]);
    NSLog(@"-[LADRemoteFile] - remoteDirectory path : %@\n\n",[LADRemoteFile getRemoteDirectoryPath]);
}

// ---------------------------------------------------------------------------------
#pragma mark - Private methods
// ---------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------
#pragma mark - FileManager methods
// ---------------------------------------------------------------------------------

/*!
 @brief Create a directory with [NSFileManager defaultManager]
 @param path the path of the directory to create
 */
+ (void)createDirectoryAtPath:(NSString*)path
{
    NSError* error;
    
    // check if directory already exists
    if(![[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:path
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:&error];
        if(error!=nil)
        {
            if ([LADRemoteFile sharedInstance].debugMode >= LADRemoteFileDebugModeError)
            {
                NSLog(@"\n\n-[LADRemoteFile] : error when create directory at path : %@ . Error description : %@\n\n",path,error.description);
            }
        }
        else
        {
            if ([LADRemoteFile sharedInstance].debugMode > LADRemoteFileDebugModeNone)
            {
                NSLog(@"\n\n-[LADRemoteFile] : remote file directory created at path : %@\n\n",path);
            }
        }
    }
}

// ---------------------------------------------------------------------------------

/*!
 @brief rename a file when the file is not valid
 @param fileName Original name of the file not valid
 */
+ (void)renameErrorFileWithName:(NSString*)fileName
{
    if(fileName == nil)
    {
        return;
    }
    
    NSError* error;
    NSFileManager* fileManager = [NSFileManager defaultManager];
    
    //Make error path
    NSString* remoteFilePath = [LADRemoteFile getFilePathFromRemoteDirectory:fileName];
    
    //Create new file path with prefix error_ (to keep a track of the error)
    NSString* errorFileName = @"error_";
    errorFileName = [errorFileName stringByAppendingString:fileName];
    NSString *errorFilePath = [LADRemoteFile getFilePathFromRemoteDirectory:errorFileName];
    
    if([fileManager fileExistsAtPath:remoteFilePath])
    {
        //S'il y a déjà un fichier d'erreur du meme nom, on le supprime pour ne pas avoir d'erreur lors du moveItemAtPath
        if([fileManager fileExistsAtPath:errorFilePath])
        {
            [fileManager removeItemAtPath:errorFilePath error:&error];

            if(error != nil)
            {
                if ([LADRemoteFile sharedInstance].debugMode >= LADRemoteFileDebugModeError)
                {
                    NSLog(@"\n\n-[LADRemoteFile] : error when removeItemAtPath : %@, error description : %@\n\n", errorFilePath, error.description);
                }
                
                error = nil;
            }
        }
        
        [fileManager moveItemAtPath:remoteFilePath toPath:errorFilePath error:&error];

        if(error != nil)
        {
            if ([LADRemoteFile sharedInstance].debugMode >= LADRemoteFileDebugModeError)
            {
                NSLog(@"\n\n-[LADRemoteFile] : error when moveItemAtPath : %@ toPath : %@,error description : %@\n\n", remoteFilePath, errorFilePath, error.description);
            }
        }
    }
    else if ([LADRemoteFile sharedInstance].debugMode >= LADRemoteFileDebugModeNone)
    {
        NSLog(@"\n\n-[LADRemoteFile] : error there is no file downloaded at path : %@\n\n", remoteFilePath);
    }
}

// ---------------------------------------------------------------------------------
#pragma mark - Utils Methods
// ---------------------------------------------------------------------------------

+ (AFHTTPSessionManager *)httpSessionManager
{
    AFHTTPSessionManager *httpManager = [AFHTTPSessionManager manager];
    httpManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    return httpManager;
}

// ---------------------------------------------------------------------------------

/*!
 @brief return string value of debug mode
 */
+ (NSString *)debugModeToString
{
    switch ([LADRemoteFile sharedInstance].debugMode)
    {
        case LADRemoteFileDebugModeNone:
            return @"LADRemoteFileDebugModeNone";
            break;
            
        case LADRemoteFileDebugModeInfo:
            return @"LADRemoteFileDebugModeInfo";
            break;
            
        case LADRemoteFileDebugModeError:
            return @"LADRemoteFileDebugModeError";
            break;
            
        case LADRemoteFileDebugModeVerbose:
            return @"LADRemoteFileDebugModeVerbose";
            break;
            
        default:
            return @"unknow";
            break;
    }
}

// ---------------------------------------------------------------------------------
#pragma mark - Ressources private Methods
// ---------------------------------------------------------------------------------

/*!
 @brief Check if the template contains values like macro to replace '_BODY_'
 @param values NSArray of string that has to be contained in template
 @param path path url of the template to check
 */
+ (BOOL)checkValues:(NSArray*)values inTemplateAtPath:(NSURL*)path
{
    // If there is no value to check we consider 'Template files' as valid
    if(values == nil || ([values isKindOfClass:[NSArray class]] && values.count == 0))
    {
        return true;
    }
    
    if([[NSFileManager defaultManager] fileExistsAtPath:path.relativePath])
    {
        NSStringEncoding encoding;
        NSString *content = [NSString stringWithContentsOfFile:path.relativePath usedEncoding:&encoding  error:NULL];
        
        for(NSString* value in values)
        {
            if(![content containsString:value])
            {
                return false;
            }
        }
        
        return true;
    }
    
    return false;
}

// ---------------------------------------------------------------------------------

/*!
 @brief return path to directory where file are write
 */
+ (NSString*)getRemoteDirectoryPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:[LADRemoteFile sharedInstance].remoteDirectoryName];
    
    return path;
}

// ---------------------------------------------------------------------------------

/*!
 @brief Get ressource path from remote directory
 @param filename ressource name we look
 @return NSString path of the ressouce if found else empty string
 */
+ (NSString*)getFilePathFromRemoteDirectory:(NSString *)filename
{
    NSString *path = [LADRemoteFile getRemoteDirectoryPath];
    NSString *filePath = [path stringByAppendingPathComponent:filename];
    
    if(filePath == nil || filePath.length == 0)
    {
        filePath = @"";
    }
    
    return filePath;
}

// ---------------------------------------------------------------------------------

/*!
 @brief Try to return filepath from app bundle for a filename. if file path not found return empty string
 @param filename Name of file we want to load
 */
+ (NSString *)getFilePathFromAppBundle:(NSString *)filename
{
    NSString *filePath;
    NSString* extension = [filename pathExtension];
    NSString* ressourceName = [filename stringByDeletingPathExtension];
    
    if ([[NSBundle mainBundle] pathForResource:ressourceName ofType:extension] != nil)
    {
        filePath = [[NSBundle mainBundle] pathForResource:ressourceName ofType:extension];
    }
    
    if (filePath == nil)
    {
        // set default value
        filePath = @"";
        
        if ([LADRemoteFile sharedInstance].debugMode >= LADRemoteFileDebugModeError)
        {
            NSLog(@"-[LADRemoteFile] - Error : path not found for filename : %@\n\n",filename);
        }
    }
    
    return filePath;
}

// ---------------------------------------------------------------------------------

/*!
 @brief get the content of a file (html,json...) completed with parameters (replace macro with provide value)
 @param filepath path of the ressource to load
 @param parameters to replace occurences of string in html. Key = occurence to replace (_BODY_),
 value = the value for the occurence (<body>bla bla</body>)
 @return A string created by reading data from the file named by path. If the file can’t be opened or there is an encoding error, returns empty string
 */
+ (NSString *)getContentFromFilePath:(NSString *)filepath withParameter:(NSDictionary *)parameters
{
    NSString *content = @"";
    NSStringEncoding encoding;
    
    if(filepath != nil && filepath.length > 0)
    {
        content = [NSString stringWithContentsOfFile:filepath  usedEncoding:&encoding  error:NULL];
    }
    else
    {
        if ([LADRemoteFile sharedInstance].debugMode >= LADRemoteFileDebugModeError)
        {
            NSLog(@"\n\n-[LADRemoteFile] ERROR : there is no local or remote template available for filepath : %@\n\n",filepath);
        }
    }
    
    // On remplace les occurences pour les templates
    for (NSString* key in parameters)
    {
        content = [content stringByReplacingOccurrencesOfString:key withString:[parameters valueForKey:key]];
    }
    
    if (content == nil)
    {
        content = @"";
    }
    
    return content;
}

// ---------------------------------------------------------------------------------

/*!
 @brief Load json obect from a path
 @param jsonPath path to ressource to load
 @return NSDictionary with json representation, return nil if ressource not found
 */
+ (NSDictionary *)getJsonAtPath:(NSString*)jsonPath
{
    NSDictionary *jsonDict;
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:jsonPath])
    {
        NSData *jsonData = [[NSData alloc] initWithContentsOfFile:jsonPath];
        
        if (jsonData != nil)
        {
            //convert JSON NSData to a usable NSDictionary
            NSError *error;
            jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData
                                                       options:0
                                                         error:&error];
            
            if (error && ([LADRemoteFile sharedInstance].debugMode >= LADRemoteFileDebugModeInfo))
            {
                NSLog(@"-[LADRemoteFile] - ERROR : failed to load json ressource %@ | desc : %@",jsonPath,error.description);
            }
        }
        else if ([LADRemoteFile sharedInstance].debugMode >= LADRemoteFileDebugModeInfo)
        {
            NSLog(@"-[LADRemoteFile] - ERROR : failed to load json ressource %@",jsonPath);
        }
    }
    
    return jsonDict;
}

// ---------------------------------------------------------------------------------

@end


